# CitizenUp.us



Site dedicated to adding ease to finding and commenting to US Government regulations on [Regulations.gov](https://www.regulations.gov/). We open up the extensive list of regulations that are open for public comment to be easily retrievable using tagging and keyword search and offer notification services to be alerted of updates to the regulations or areas of political concern that you follow. 